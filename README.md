# Welcome to the flights_api_v1 web service

The API is accessible via:
http://flights-api-v1app.uzb68m4upm.eu-central-1.elasticbeanstalk.com

The Grafana Dashboard:
http://35.157.107.174:3000/d/-PaMj1Uiz/flights-api-dashboard?refresh=1m&orgId=1

Prometheus:
http://18.184.185.0:9090/graph

## Swagger Editor:

```
docker pull swaggerapi/swagger-editor
docker run -p 80:8080 swaggerapi/swagger-editor
```

A description of the API can be found in ./config/flights_api_swagger.yaml

## Build Docker image:
```
docker build -t flights-api-v1 .
docker run flights-api-v1:latest
```

https://medium.com/@journald/prometheus-on-ecs-with-mongodb-exporter-8b58dad1ee8f