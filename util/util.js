'use strict';

const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = 'debug';

const moment = require('moment');
const AWS = require('aws-sdk');
const config = require('../config/config.js');

function checkFlightsInput(body){
    if (body.start && body.end && body.departure && body.aircraft){

        // all required parameters exist
        // now check aircraft type and departure
        if (!moment(body.departure, moment.ISO8601, true).isValid()){
            return false;
        }

        if (!(body.aircraft === 'DHC-8-400' || body.aircraft === 'Boing B737' || body.aircraft === 'Airbus A340')){
            return false;
        }

        return true;

    } else {
        // s.th. is missing
        return false;
    }
};

function getAWSConfig(){
    if (process.env.NODE_ENV === 'production'){
        return config.aws_remote_config;
    } else if (process.env.NODE_ENV === 'gitlab'){
        return config.aws_gitlab_config;
    } else {
        return config.aws_local_config;
    }
}

// async function, needs to return a Promise
// TODO: this flight number is not unique yet
function getFlightNumber(docClient, start, end, date){

    return new Promise((resolve, reject) => {

        // propose a flight_number
        const flight_number = start + end + '_'
              + date.getFullYear()
              + (date.getMonth() + 1)
              + date.getDate()
              + date.getHours();

        const params = {
            TableName: config.aws_table_name,
        };

        docClient.scan(params, function(err, data) {
            if (err) {
                // error handling
                reject(err);
            } else {
                console.log('data', data);
                const { Count } = data;

                resolve(flight_number + '_' + Count);
            }
        });
    });
}

function createTable(){

    logger.debug('util:createTable:start');

    var status = 'false';

    return new Promise((resolve, reject) => {

        logger.debug('util:createTable:getAWSConfig: ' + JSON.stringify(getAWSConfig(), null, 2));
        AWS.config.logger = console;

        AWS.config.update(getAWSConfig());

        logger.debug('util:createTable:AWSConfig: ' + JSON.stringify(AWS.config, null, 2));

        const dynamodb = new AWS.DynamoDB();

        var params = {
            TableName: config.aws_table_name, /* required */
        };

        dynamodb.describeTable(params, function(err, data) {
            if (err) {
                status = 'false';
                logger.debug('err: ' + err.message); // an error occurred

                // if table does not exist, create it
                var params_table = require('../config/dynamodb/tables.json');

                dynamodb.createTable(params_table, function(err, data){

                    if (err) {
                        logger.debug('err: ' + err.message); // an error occurred
                        reject(err);
                    } else {
                        status = ('true');
                        logger.debug(data); // successful response
                        logger.debug('STATUS===========>' + status);
                        resolve(status);
                    }
                });
            } else {
                status = ('true');
                logger.debug(data); // successful response
                logger.debug('STATUS===========>' + status);
                resolve(status);
            }

        });
    });
}

exports.checkFlightsInput = checkFlightsInput;
exports.getAWSConfig = getAWSConfig;
exports.getFlightNumber = getFlightNumber;
exports.createTable = createTable;
