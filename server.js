'use strict';

var port = process.env.PORT || 5000;

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const flightsRouter = require('./routes/flights');
const prometheus = require('./routes/prometheus');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./config/flights_api_swagger.json');

const util = require('./util/util.js');

const app = express();
const basicAuth = require('express-basic-auth');

util.createTable()
    .then(status => {

        console.log('starting api');

        // view engine setup
        app.set('views', path.join(__dirname, 'views'));
        app.set('view engine', 'pug');

        // basic authentication
        app.use('/v1', basicAuth({
            users: { admin: 'supersecret' },
            challenge: true,
        }));

        // setup prometheus metrics
        // The below arguments start the counter functions
        app.use(prometheus.requestCounters);
        app.use(prometheus.responseCounters);

        // Enable collection of default metrics
        prometheus.startCollection();

        app.use(logger('dev'));
        app.use(express.json());
        app.use(express.urlencoded({ extended: false }));
        app.use(cookieParser());
        app.use(express.static(path.join(__dirname, 'public')));

        app.use('/', indexRouter);
        app.use('/users', usersRouter);
        app.use('/v1/flights/', flightsRouter);

        // add route to swagger ui
        app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

        // Enable metrics endpoint
        prometheus.injectMetricsRoute(app);

        // catch 404 and forward to error handler
        app.use(function(req, res, next) {
            next(createError(404));
        });

        // error handler
        app.use(function(err, req, res, next) {
            // set locals, only providing error in development
            res.locals.message = err.message;
            res.locals.error = req.app.get('env') === 'development' ? err : {};

            // render the error page
            res.status(err.status || 500);
            res.render('error');
        });

        app.listen(port);

        app.emit('ApiStarted');

    })
    .catch(err => {
        console.log(err, err.stack); // an error occurred
    });

module.exports = app;
