'use strict';

const supertest = require('supertest');
const test = require('unit.js');
const app = require('../server.js');

const request = supertest(app);

before(function(done) {
    app.on('ApiStarted', done);
});

describe('Tests /v1/flights', function() {

    it('verifies get without basic authentication', function(done) {
        request
            .get('/v1/flights')
            .expect(401)
            .end(function(err, result) {
                test
                    .value(result)
                    .hasHeader('www-authenticate', 'Basic');
                done(err);
            });
    });

    it('verifies get with basic authentication', function(done) {
        request
            .get('/v1/flights')
            .auth('admin', 'supersecret')
            .expect(200)
            .end(function(err, result) {
                test
                    .value(result)
                    .hasHeader('content-type', 'application/json; charset=utf-8');
                done(err);
            });
    });

    //    it('verifies post', function(done) {
    //        request
    //            .post('/')
    //            .expect(200)
    //            .end(function(err, result) {
    //                test.string(result.body.Output).contains('Hello');
    //                test.value(result).hasHeader('content-type', 'application/json; charset=utf-8');
    //                done(err);
    //            });
    //    });

});
