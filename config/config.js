'use strict';

module.exports = {
    aws_table_name: 'flights-dev',
    aws_local_config: {
        region: 'local',
        accessKeyId: 'foo',
        secretAccessKey: 'bar',
        endpoint: 'http://localhost:8000',
    },
    aws_gitlab_config: {
        region: 'local',
        accessKeyId: 'foo',
        secretAccessKey: 'bar',
        endpoint: 'http://amazon-dynamodb-local:8000',
    },
    aws_remote_config: {
        region: process.env.region,
        accessKeyId: process.env.accessKeyId,
        secretAccessKey: process.env.secretAccessKey,
    },
};
