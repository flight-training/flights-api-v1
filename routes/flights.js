'use strict';

var express = require('express');
var router = express.Router();

const AWS = require('aws-sdk');
const config = require('../config/config.js');
const util = require('../util/util.js');

/* Add a flight. */
router.post('/', async function(req, res, next) {

    AWS.config.update(util.getAWSConfig());

    // Check input, reject if incomplete
    if (util.checkFlightsInput(req.body) !== true) {
        res.status(422).send({
            success: false,
            message: 'Error: Input error!',
        });

        // stop execution
        return;
    }

    const { start, end, departure, aircraft } = req.body;
    const docClient = new AWS.DynamoDB.DocumentClient();

    // Not actually unique and can create problems.
    // const flight_number = (Math.random() * 1000).toString();

    var flight_number;
    await util.getFlightNumber(docClient, start, end, new Date(departure))
        .then(rv => {
            flight_number = rv;
        })
        .catch(err => {
            res.send({
                success: false,
                message: 'Error: Server error 1: ' + err.message,
            });

            // stop execution
            return;
        });

    console.log('flight_number: ' + flight_number);

    const params = {
        TableName: config.aws_table_name,
        Item: {
            flight_number: flight_number,
            start: start,
            end: end,
            departure: departure,
            aircraft: aircraft,
        },
    };

    docClient.put(params, function(err, data) {
        if (err) {
            res.send({
                success: false,
                message: 'Error: Server error: ' + err.message,
            });
        } else {
            console.log('data', data);

            res.send({
                success: true,
                message: 'Added flight',
                Location: '/v1/flights/' + flight_number,
            });
        }
    });
});


/* GET flights listing. */
router.get('/', function(req, res, next) {

    AWS.config.update(util.getAWSConfig());

    const docClient = new AWS.DynamoDB.DocumentClient();

    const params = {
        TableName: config.aws_table_name,
    };

    docClient.scan(params, function(err, data) {
        if (err) {
            res.send({
                success: false,
                message: 'Error: Server error!',
            });
        } else {
            const { Items } = data;

            res.send({
                success: true,
                message: 'Loaded flights',
                flights: Items,
            });
        }
    });

});

/* GET a specific flight. */
router.get('/:flight_number', function(req, res, next) {

    AWS.config.update(util.getAWSConfig());

    const flight_number = req.params.flight_number;
    console.log('flight_number: ' + flight_number);

    const docClient = new AWS.DynamoDB.DocumentClient();

    const params = {
        TableName: config.aws_table_name,
        Key: { flight_number: flight_number },
    };

    docClient.get(params, function(err, data) {
        if (err) {
            res.send({
                success: false,
                message: 'Error: Server error',
            });
        } else {
            console.log('data', data);
            const { Item } = data;

            if (Item){
                res.send({
                    flights: Item,
                });
            } else {
                res.status(403).send({
                    message: 'Flight ' + flight_number + ' not found.',
                });
            }
        }
    });

});

/* DELETE flight. */
router.delete('/:flight_number', function(req, res, next) {

    AWS.config.update(util.getAWSConfig());

    const flight_number = req.params.flight_number;

    const docClient = new AWS.DynamoDB.DocumentClient();

    const params = {
        TableName: config.aws_table_name,
        Key: { flight_number: flight_number },
    };

    docClient.delete(params, function(err, data) {
        if (err) {
            res.send({
                success: false,
                message: 'Error: Server error',
            });
        } else {
            res.send({
                success: true,
                message: 'Deleted flight',
            });
        }
    });

});


module.exports = router;
