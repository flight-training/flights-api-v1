'use strict';

let express = require('express');
let router = express.Router();

let APIURL = process.env.APIURL;
let GRAFANAURL = process.env.GRAFANAURL;

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'flights api', APIURL: APIURL, GRAFANAURL: GRAFANAURL });
});

module.exports = router;
